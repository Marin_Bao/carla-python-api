
"""
Author: Kin Zhang <kin_eng@163.com>

此代码主要用于全局路径规划的使用，与follow不同的是 直接使用了内置的behavior_agent.py 更直接点
1. 运行carla
2. 运行，其中args为输入的出身点和目标点，如果随机的话 请设为None
3. 由 # Author: Runsheng Xu <rxx3386@ucla.edu> License: MIT 修改而来
4. 注意第127行：# 不同的版本 这行可能需要替换一下（发现10和11就是不同的behavior_agent.py 主要是看是否需要输入vehicle）
"""



import glob
import os
import sys
# ==============================================================================
# -- Find CARLA module ---------------------------------------------------------
# ==============================================================================
try:
    sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    print('please check whether the egg in path')
    pass
import carla
# ==============================================================================
# -- Add PythonAPI for release mode --------------------------------------------
# ==============================================================================
try:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/carla')
except IndexError:
    print('please check whether the PythonAPI lib in path')
    pass

from agents.navigation.behavior_agent import BehaviorAgent

import random

# args
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--host', metavar='H',       default='127.0.0.1', help='IP of the host server (default: 127.0.0.1)')
parser.add_argument('--port', '-p', metavar='P', default=2000, type=int, help='TCP port to listen to (default: 2000)')
parser.add_argument('--tm-port', '-tmp',         default=8000, type=int, help='Traffic Manager Port (default: 8000)')
# For Town04 Map
parser.add_argument('--ego-spawn',               default=[5.565537, 128.869553], help='[x,y] in world coordinate')
parser.add_argument('--target',                  default=[400.00, -374.479], help='[x,y] in world coordinate')
parser.add_argument('--top-view',                default=True, help='Top view of ego car')
parser.add_argument('--map',                     default='Town04', help='Town Map')
parser.add_argument('--sync',                    default=True, help='Synchronous mode execution')
args = parser.parse_args()

def main(args):
    try:
        client = carla.Client(args.host, args.port)
        client.set_timeout(10.0)


        world = client.get_world()
        world_map = world.get_map()

        # set map
        if args.map and world_map.name != args.map:
            world = client.load_world(args.map)
            world_map = world.get_map()
            print("Town Map changing, ReloadMap: ", args.map)

        # Retrieve the world that is currently running
        world = client.load_world(args.map)
        origin_settings = world.get_settings()

        if args.sync:
            settings = world.get_settings()
            traffic_manager = client.get_trafficmanager(args.tm_port)
            traffic_manager.set_synchronous_mode(True)
            if not settings.synchronous_mode:
                settings.synchronous_mode = True
                settings.fixed_delta_seconds = 0.05
                world.apply_settings(settings)

        blueprint_library = world.get_blueprint_library()

        # read all valid spawn points
        all_default_spawn = world_map.get_spawn_points()
        
        if args.ego_spawn is not None:
            spawn_point = carla.Transform(carla.Location(x=args.ego_spawn[0], y=args.ego_spawn[1], z=2))
        else:# randomly choose one as the start point
            spawn_point = random.choice(all_default_spawn) if all_default_spawn else carla.Transform()

        
        spawn_point = world_map.get_waypoint(spawn_point.location).transform
        # need height otherwise will have collision
        spawn_point.location.z = 2

        # create the blueprint library
        ego_vehicle_bp = blueprint_library.find('vehicle.lincoln.mkz2017')
        vehicle = world.spawn_actor(ego_vehicle_bp, spawn_point)

        # we need to tick the world once to let the client update the spawn position
        world.tick()

        # create the behavior agent
        agent = BehaviorAgent(vehicle, behavior='normal')
        # print(args.target)
        if args.target is not None:
            destination = carla.Transform(carla.Location(x=args.target[0], y=args.target[1], z=2))
        else:
            # set the destination spot
            spawn_points = world.get_map().get_spawn_points()
            random.shuffle(spawn_points)

            # to avoid the destination and start position same
            if spawn_points[0].location != agent.vehicle.get_location():
                destination = spawn_points[0]
            else:
                destination = spawn_points[1]
        destination = world_map.get_waypoint(destination.location).transform
        # generate the route
        agent.set_destination(agent.vehicle.get_location(), destination.location, clean=True)

        while True:
            # 不同的版本 这行可能需要替换一下
            agent.update_information(vehicle)

            world.tick()

            # if you want to change the speed limit
            # default like this one
            # agent.speed_limit = world.get_speed_limit()
            # agent._local_planner.set_speed(agent.speed_limit)

            # get_speed_limit(self): The client returns the speed limit **affecting this vehicle** according to last tick 
            # (it does not call the simulator). The speed limit is updated when passing by a speed limit signal, so a 
            # vehicle might have none right after spawning.
            # Return: float - m/s

            if len(agent._local_planner.waypoints_queue)<1:
                print('======== Success, Arrivied at Target Point!')
                break
            
            # top view
            spectator = world.get_spectator()
            transform = vehicle.get_transform()
            spectator.set_transform(carla.Transform(transform.location + carla.Location(z=40),
                                                    carla.Rotation(pitch=-90)))

            speed_limit = vehicle.get_speed_limit()
            agent.get_local_planner().set_speed(speed_limit)

            control = agent.run_step(debug=True)
            vehicle.apply_control(control)

    finally:
        world.apply_settings(origin_settings)
        vehicle.destroy()


if __name__ == '__main__':
    try:
        main(args)
    except KeyboardInterrupt:
        print(' - Exited by user.')