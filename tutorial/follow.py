
"""
此代码主要用于全局路径规划的使用：
1. 运行carla
2. 运行此代码，其中已经建立了循环目标点
"""

import glob
import os
import sys

# ==============================================================================
# -- Find CARLA module ---------------------------------------------------------
# ==============================================================================
try:
    sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass

# ==============================================================================
# -- Add PythonAPI for release mode --------------------------------------------
# ==============================================================================
try:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/carla')
except IndexError:
    pass

import carla

import math
import numpy as np
import pandas as pd

from agents.navigation.global_route_planner import  GlobalRoutePlanner  # pylint: disable=import-error
from agents.navigation.global_route_planner_dao import GlobalRoutePlannerDAO   # pylint: disable=import-error
from agents.navigation import controller    # pylint: disable=import-error

import matplotlib.pyplot as plt


TOTAL_EPISODE_FRAMES = 100

def spawn_vehicle(client, spawnPoint):
    
    """
    
    This function spawn vehicles in the given spawn points. If no spawn 
    point is provided it spawns vehicle in this 
    position x=27.607,y=3.68402,z=0.02
    """
    
    spawnPoint=spawnPoint
    world = client.get_world()
    blueprint_library = world.get_blueprint_library()
    bp = blueprint_library.find('vehicle.lincoln.mkz2017')
    vehicle = world.spawn_actor(bp, spawnPoint)
    return vehicle

def find_dist_veh(vehicle_loc,target):
    dist = math.sqrt( (target.transform.location.x - vehicle_loc.x)**2 + (target.transform.location.y - vehicle_loc.y)**2 )
    
    return dist
    
def setup_PID(vehicle, setting):
    
    
    """
    This function creates a PID controller for the vehicle passed to it 
    """

    args_lateral_dict = {
            'K_P': 2,
            'K_D': 0.2,
            'K_I': 0.07,
            'dt': setting.fixed_delta_seconds
            }

    args_long_dict = {
            'K_P': 1,
            'K_D': 0.0,
            'K_I': 0.75,
            'dt': setting.fixed_delta_seconds
            }

    PID = controller.VehiclePIDController(vehicle,args_lateral=args_lateral_dict,args_longitudinal=args_long_dict)
    
    return PID

def get_waypoints(grp, spawn_points, world, vehicle_sp, end=100, set_point=False):
    FALG_ZERO_END = False
    if set_point:
        end_point = []
        end_point.append(carla.Location(x=5.565537, y=128.869553)) # start point
        end_point.append(carla. Location(x=400, y=-374.479401)) # first
        end_point.append(carla. Location(x=392.240143, y=-136.115814)) # first -second
        end_point.append(carla.Location(x=-488.061005, y=44.639484)) # second
        a = vehicle_sp
        if int(end/100)>len(end_point)-1:
            end = 0
        b = end_point[int(end/100)]
        
    else:
        if end>len(spawn_points):
            end = len(spawn_points)-1
            FALG_ZERO_END = True
        a = vehicle_sp
        b = carla.Location(spawn_points[end].location)
    world.debug.draw_point(a,color=carla.Color(r=255, g=255, b=0),size=1 ,life_time=300.0)
    world.debug.draw_point(b,color=carla.Color(r=255, g=255, b=0),size=1 ,life_time=300.0)
    w1 = grp.trace_route(a, b) 
    wps=[]
    for i in range(len(w1)):
        wps.append(w1[i][0])
        world.debug.draw_point(w1[i][0].transform.location,color=carla.Color(r=255, g=0, b=0),size=0.4 ,life_time=300.0)
    return wps, FALG_ZERO_END

fig, axs = plt.subplots(4, figsize=(5,10))
def live_plot(x,y1,y2,t,s,b):
    plt.cla()
    line1, =axs[0].plot(x, y2, "b-")
    line2, =axs[0].plot(x, y1, "r-")
    axs[0].set_title('Speed',size=5)
    if len(y1)<3:
        line1.set_label('target')
        line2.set_label('current')
        axs[0].legend()
    axs[1].plot(x, s)
    axs[1].set_title('Steer',size=5)
    axs[2].plot(x, t)
    axs[2].set_title('Throttle',size=5)
    axs[3].plot(x, b)
    axs[3].set_title('Brake',size=5)
    plt.axis("equal")
    plt.grid(True)
    plt.pause(0.0001)

def main():
    try:
        top_view = True
        loop_time = 1
        client = carla.Client("localhost", 3000)
        client.set_timeout(10)
        world = client.load_world('Town04')
        world = client.get_world()

        origin_settings = world.get_settings()

        # set sync mode
        settings = world.get_settings()
        settings.synchronous_mode = True
        settings.fixed_delta_seconds = 0.05
        world.apply_settings(settings)
        current_timestamp = 0

        AllMap = world.get_map()
        sampling_resolution = 4
        dao = GlobalRoutePlannerDAO(AllMap, sampling_resolution)
        grp = GlobalRoutePlanner(dao)
        grp.setup()
        spawn_points = world.get_map().get_spawn_points()
        vehicle_sp = carla.Transform(carla.Location(x=5.565537, y=128.869553, z=2))
        # vehicle_sp = spawn_points[0]
        end_id = 100
        # wps = get_waypoints(grp, spawn_points, world,vehicle_sp, start_id, end_id)
        wps,FALG_ZERO_END = get_waypoints(grp, spawn_points, world,vehicle_sp.location, end_id,set_point=True)
        spawn_v_point = vehicle_sp
        vehicle=spawn_vehicle(client,spawn_v_point)
        # we need to tick the world once to let the client update the spawn position
        world.tick()
        PID=setup_PID(vehicle, settings)

        speed_lim = vehicle.get_speed_limit()
        
        
        record_wp_num = 0
        target=wps[0]
        if not top_view:
            spectator = world.get_spectator()
            spectator.set_transform(carla.Transform(carla.Location(x=spawn_v_point.loacation.x,y=spawn_v_point.loacation.y,z=176),
                                                        carla.Rotation(pitch=-73,yaw=-89,roll=0)))

        hspeed_lim = speed_lim
        hspeed_kmh = 0
        htimestamp = 0
        hthrottle = 0
        hsteer = 0
        hbrake = 0
        while True:
            vehicle_loc= vehicle.get_location()
            distance_v =find_dist_veh(vehicle_loc,target)
            control = PID.run_step(speed_lim,target)
            vehicle.apply_control(control)

            if top_view:
                spectator = world.get_spectator()
                transform = vehicle.get_transform()
                spectator.set_transform(carla.Transform(transform.location + carla.Location(z=40),
                                                        carla.Rotation(pitch=-90)))
            else:
                begin = vehicle.get_transform().location + carla.Location(z=5)
                end = begin + carla.Location(z=0)
                world.debug.draw_arrow(begin, end, arrow_size=0.5, life_time=1.0)
            
            if record_wp_num==(len(wps)-1):
                print("last waypoint reached, point id:",end_id)
                loop_time = loop_time - 1
                if loop_time is not 0:
                    end_id = end_id+100
                    wps, FALG_ZERO_END = get_waypoints(grp, spawn_points, world,vehicle_loc, end_id,set_point=True)
                    target=wps[0]
                    record_wp_num = 0
                    if FALG_ZERO_END:
                        end_id=100
                else:
                    data = {'speed':hspeed_kmh,
                            'target':hspeed_lim,
                            'time':htimestamp,
                            'throttle':hthrottle,
                            'steer':hsteer,
                            'brake':hbrake}
                    df = pd.DataFrame(data, columns= ['speed', 'target','time','throttle','steer','brake'])
                    df.to_csv (r'C:\Users\zhangqingwen\Downloads\AProgramm\export.csv', index = False, header=True)
                    break
            
            # change to next waypoint
            if (distance_v < sampling_resolution):
                control = PID.run_step(speed_lim,target)
                vehicle.apply_control(control)
                record_wp_num=record_wp_num+1
                target=wps[record_wp_num]
            
            current_timestamp = current_timestamp + settings.fixed_delta_seconds
            v_speed = vehicle.get_velocity()
            speed_kmh = (3.6 * math.sqrt(v_speed.x**2 + v_speed.y**2 + v_speed.z**2))
            speed_lim = vehicle.get_speed_limit() * 0.8

            # collect data
            hspeed_kmh = np.hstack((hspeed_kmh, speed_kmh))
            hspeed_lim = np.hstack((hspeed_lim, speed_lim))
            htimestamp = np.hstack((htimestamp, current_timestamp))
            hthrottle = np.hstack((hthrottle, vehicle.get_control().throttle))
            hsteer = np.hstack((hsteer, vehicle.get_control().steer))
            hbrake = np.hstack((hbrake, vehicle.get_control().brake))
            # live_plot(htimestamp,hspeed_kmh,hspeed_lim,hthrottle,hsteer,hbrake)
            world.tick()

    finally:
        world.apply_settings(origin_settings)
        vehicle.destroy()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print(' - Exited by user.')