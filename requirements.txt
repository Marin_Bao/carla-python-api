# exmaple requirements
future
numpy
pygame
matplotlib
open3d

# carla folder requirements
networkx
distro

# util folder requirements
psutil
py-cpuinfo
python-tr

# tutorial requirements
hydra-core>=1.1
tqdm
